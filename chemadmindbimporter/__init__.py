import psycopg2
import psycopg2.sql as sql
import datetime
import getpass

psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)


def insert_record(
    table, record, connect, do_not_return_id=False, confirm=True, primary_key_name="id"
):
    """
    Inserts a dictionary of column/values into a table in the database.
    By default, returns the 'id' column of the inserted entry. That can be
    changed by setting primary_key_name or do_not_return_id . It is up to
    the caller to ensure that the dictionary being passed contains suitable
    values.
    """
    cols = list(record.keys())
    q = sql.SQL("insert into {} ({}) values ({}) returning *").format(
        sql.Identifier(table),
        sql.SQL(", ").join(map(sql.Identifier, cols)),
        sql.SQL(", ").join(map(sql.Placeholder, cols)),
    )
    with connect:
        with connect.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
            print("About to execute the following SQL:")
            print(cur.mogrify(q, record))
            answer = "Y"
            if confirm:
                answer = getpass.getpass("Press n to skip:")
            if answer != "n":
                cur.execute(q, record)
                inserted = cur.fetchone()
    if do_not_return_id:
        return None
    # FIXME need better error handling here but we've already committed the transaction
    return inserted[primary_key_name]


def update_record(table, newrecord, connect, confirm=True, primary_key_name="id"):
    """
    Updates an existing record in the database.
    Takes a dictionary which must contain a suitable key to locate the record
    to update (by default the 'id' key). Bails if more than one record found.
    It is up to the caller to ensure that the dictionary passed contains suitable
    values.
    """
    updated = False
    q1 = sql.SQL("select * from {} where {} = {}").format(
        sql.Identifier(table), sql.Identifier(primary_key_name), sql.Placeholder()
    )
    with connect:
        with connect.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
            cur.execute(q1, [newrecord[primary_key_name]])
            if cur.rowcount == 1:
                oldrecord = cur.fetchone()
                for col in list(newrecord.keys()):
                    if oldrecord[col] != newrecord[col]:
                        q = sql.SQL("update {} set {} = {} where {} = {}").format(
                            sql.Identifier(table),
                            sql.Identifier(col),
                            sql.Placeholder(),
                            sql.Identifier(primary_key_name),
                            sql.Placeholder(),
                        )
                        print("About to execute the following SQL:")
                        print(
                            cur.mogrify(
                                q, [newrecord[col], newrecord[primary_key_name]]
                            )
                        )
                        answer = "Y"
                        if confirm:
                            answer = getpass.getpass("Press n to skip")
                        if answer != "n":
                            cur.execute(
                                q, [newrecord[col], newrecord[primary_key_name]]
                            )
                            updated = True
            elif cur.rowcount == 0:
                print(
                    "Could not find record with {} set to {}".format(
                        primary_key_name, newrecord[primary_key_name]
                    )
                )
            else:
                print(
                    "Found more than one record with {} set to {}".format(
                        primary_key_name, newrecord[primary_key_name]
                    )
                )
    return updated
