#!/usr/bin/env python
# This needs the chemadmindbimporter Python module, which is in git.
# It is not part of chemaccmgmt because it relies on a very recent version
# of psycopg2. See its README for how to make it go on a managed Linux machine. 
#
# You will need to run this in dryrun mode a few times, edit the data to remove
# typos - or possibly edit chemaccmgmt.importers to add new supervisors and 
# spelling mistakes - and only then run it live. 
import chemadmindbimporter
import datetime
import chemaccmgmt
from chemaccmgmt import importers
import csv,sys,pprint,os,re

# FIXME update these each year, see also further down
filemaker_admissions = '2017-data/cdt.csv'
start_date = datetime.date(2017,10,1)

dryrun = False

separator = '##########################################################################'

def csv_to_dict(f):
    reader = csv.reader(f)
    header = reader.next()
    outpt = []
    for r in reader:
        data = dict(zip(header,r))
        outpt.append(data)
    return(outpt)
        

# open csv file and loop over them

f = open(filemaker_admissions)
filemakerd = csv_to_dict(f)

for fh in [f]:
    fh.close()

db_handle = chemaccmgmt.db.ChemDB(username='cen1001',database='chemistry')
conn = db_handle.conn
cur = db_handle.cursor
supervisors_table = importers.supervisors_lookup

for record in filemakerd:
    # FIXME depending what columns you get this may need tweaks
    # put in the easy stuff
    person = {}
    pg = {}
    for key in ['filemaker_fees_funding','filemaker_funding','university','emplid_number']:
        if record[key].strip():
            pg[key] = record[key]
    dob_string = importers.fix_date_string(record['date_of_birth'])
    person['date_of_birth'] = datetime.datetime.strptime(dob_string,'%Y-%m-%d').date()
    pg['start_date'] = start_date

    # process the name
    person['surname'] = record['surname'].strip()
    person['crsid'] = record['crsid'].strip()
    person['first_names'] = record['first_names'].strip()
    person['email_address'] = person['crsid'] + '@cam.ac.uk'

    # nationalities
    nationality_ids = importers.get_nationality_ids(record['nationality'],db_handle)

    # course type
    if re.search('CPGS',record['type'],re.IGNORECASE):
        pg['postgraduate_studentship_type_id'] = '3'
    elif re.search('M[.]?[Pp]hil',record['type'],re.IGNORECASE):
        pg['postgraduate_studentship_type_id'] = '2'
        pg['mphil_date_submission_due'] = datetime.date(2018,8,31) #FIXME
    elif re.search('M[.]?Sc',record['type'],re.IGNORECASE):
        pg['postgraduate_studentship_type_id'] = '4'
    else:
        pg['postgraduate_studentship_type_id']='1'
        pg['first_year_probationary_report_due'] = datetime.date(2018,6,29) # FIXME

    # supervisor
    supervisor_ids = importers.get_supervisor_ids(record['supervisor'])
    pg['first_supervisor_id'] = supervisor_ids[0]
    if len(supervisor_ids) > 1:
        pg['second_supervisor_id'] = supervisor_ids[1]
    #if record['external_co_supervisor'].strip():
    #    pg['external_co_supervisor']=record['external_co_supervisor'].strip()


    college_id = importers.get_college_id(record['college'],db_handle)
    if college_id:
        person['cambridge_college_id'] = college_id

    # research groups
    rg_ids = []
    for sup in supervisor_ids:
        sql = "select id from research_group where head_of_group_id = %s and internal_use_only = 'f'"
        cur.execute(sql,[sup])
        if cur.rowcount != 0:
            rg_ids = [x[0] for x in cur]
    
    print separator
    #print 'xx' + record['home_address'] + 'xx'
    pprint.pprint(person)
    pprint.pprint(pg)
    pprint.pprint(nationality_ids)
    pprint.pprint(rg_ids)
    print separator


    # now check if the person was already there
    sql='select id from person where crsid = %s or email_address = %s'
    cur.execute(sql,[person['crsid'],person['email_address']])
    if cur.rowcount == 0:
        pprint.pprint(person)
        if not dryrun:
            person_id=chemadmindbimporter.insert_record('person',person,conn,confirm=True,do_not_return_id=False)
        else:
            person_id=0
            print 'Would insert new person'
            pprint.pprint(person)
    else:
        person_id=cur.fetchone()[0]
        person['id']=person_id
        print 'Need to update person'
        pprint.pprint(person)
        if not dryrun:
            chemadmindbimporter.update_record('person',person,conn,confirm=True)

    # nationalities
    for nat_id in nationality_ids:
        sql='select * from mm_person_nationality where person_id = %s and nationality_id = %s' 
        cur.execute(sql,[person_id,nat_id])
        if cur.rowcount==0 and not dryrun:
            print 'Updating nationality %s %s' %(person_id,nat_id)
            chemadmindbimporter.insert_record('mm_person_nationality',{'person_id':person_id,'nationality_id':nat_id},conn,confirm=False,do_not_return_id=True)
        else:
            print 'Would update nationality %s %s' %(person_id,nat_id)

    # rgs
    for rg_id in rg_ids:
        sql='select * from mm_person_research_group where person_id = %s and research_group_id = %s'
        cur.execute(sql,[person_id,rg_id])
        if cur.rowcount==0 and not dryrun:
            chemadmindbimporter.insert_record('mm_person_research_group',{'person_id':person_id,'research_group_id':rg_id},conn,confirm=False,do_not_return_id=True)
        else:
            print 'Would update rg %s %s' %(person_id,rg_id)
  
    sql='select id from postgraduate_studentship where person_id = %s and postgraduate_studentship_type_id = %s'
    cur.execute(sql,[person_id,pg['postgraduate_studentship_type_id']])
    if cur.rowcount == 0:
        pg['person_id']=person_id
        if not dryrun:
            chemadmindbimporter.insert_record('postgraduate_studentship',pg,conn,confirm=True)
        else:
            print 'Would insert pg record'
            pprint.pprint(pg)

    print separator
    print separator

sys.exit(0)
