#!/usr/bin/python3
import chemaccmgmt,re,csv
# Don't use this - create-new-postgrads-from-database replaces it


def csv_to_dict(f):
    reader=csv.reader(f)
    header=reader.next()
    outpt=[]
    for r in reader:
        data=dict(zip(header,r))
        outpt.append(data)
    return(outpt)


conn=chemaccmgmt.db.ChemDB(username='cen1001')
cur=conn.cursor
new_accounts=[]

f=open('2017-data/cdt.csv')

filemakerd=csv_to_dict(f)
f.close()

for record in filemakerd:
    crsid=record['crsid']

    cur.execute('select surname, first_names, coalesce(research_group.active_directory_container,research_group.name,$$Visitors$$) from person left join mm_person_research_group on person.id = mm_person_research_group.person_id left join research_group on mm_person_research_group.research_group_id = research_group.id where crsid = %s order by research_group_id',[crsid])
    row=cur.fetchone()
    new_accounts.append( 'sudo add-ad-user -f "%s" -s "%s" -x 2018-09-30 -q -r "%s" %s' % ( row[1],row[0],row[2],crsid))
    
print('\nsleep 30\n'.join(new_accounts))

