import chemaccmgmt
import chemadmindbimporter


d = chemaccmgmt.db.ChemDB(username='cen1001',database='chemistry_sandpit')


building = { 'building_hid' : 'Chemistry of Cake',
             'email_abbreviation' : 'cake',
           }
Id = chemadmindbimporter.insert_record('building_hid',building,d.conn,primary_key_name='building_id')
print Id

building = { 'building_hid' : 'Chemistry of Sponge Cake',
             'email_abbreviation' : 'scake',
             'building_id' : Id }
chemadmindbimporter.update_record('building_hid',building,d.conn,primary_key_name='building_id')
